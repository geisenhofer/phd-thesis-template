\chapter{Supersonic Compressible Flow}	\label{ch:gov_eq}
\glsresetall

\section{The Euler Equations}	\label{sec:EulerEquations}
The Euler equations are a physical and mathematical model for inviscid compressible flow and can be derived from the Navier-Stokes equations for large Reynolds numbers
\begin{align}
	\gls{re} = \frac{\gls{rho}_\infty \velocity_\infty \gls{l}_\infty}{\gls{viscosity}_\infty}
	\rightarrow \infty\,,
\end{align}
where $\gls{rho}_\infty$, $\velocity_\infty$, $\gls{l}_\infty$, and $\gls{viscosity}_\infty$ denote characteristic, usually free-stream, values of the density, the velocity, the length scale, and the dynamic viscosity, respectively. A detailed derivation can be found, for example, in the textbooks by \textcite{anderson1990, spurk2010}.


\subsection{Conservative Form With Dimensions}	\label{sec:gov_eq_cons_form}
We consider the two-dimensional Euler equations, which consist of the conservation laws for mass, momentum, and energy, in a differential conservative form
\begin{align}	\label{eq:eulerEquations}
	\frac{\partial \gls{U}}{\partial \gls{t}} + \frac{\partial \gls{F}_1(\gls{U})}{\partial \x_1} + \frac{\partial \gls{F}_2(\gls{U})}{\partial \x_2} = 0\,.
\end{align}
Here, $\gls{U} \in \mathbb{R}^4$ is the state vector of conserved quantities 
\begin{align}	\label{eq:conservedQuantities}
	\gls{U} =	\left( \begin{array}{c}
		\gls{rho} \\
		\gls{rho} \velocity_1\\
		\gls{rho} \velocity_2\\
		\gls{rhoE}
	\end{array} \right)\,,
\end{align}
and $\gls{F}_1(\gls{U}): \mathbb{R}^4 \rightarrow \mathbb{R}^4$ and $\gls{F}_2(\gls{U}): \mathbb{R}^4 \rightarrow \mathbb{R}^4$ are the convective flux vectors
\begin{align}	\label{eq:convectiveFluxes}
	\gls{F}_1(\gls{U}) =\left( \begin{array}{c}
		\gls{rho} \velocity_1\\
		\gls{rho} \velocity_1 \velocity_1 + \gls{p}\\
		\gls{rho} \velocity_1 \velocity_2\\
		\velocity_1 (\gls{rhoE} + \gls{p})
	\end{array} \right)\,,
	\qquad
	\gls{F}_2(\gls{U}) =\left( \begin{array}{c}
		\gls{rho} \velocity_2\\
		\gls{rho} \velocity_1 \velocity_2\\
		\gls{rho} \velocity_2 \velocity_2 + \gls{p}\\
		\velocity_2 (\gls{rhoE} + \gls{p})
	\end{array} \right)\,.
\end{align}
In \crefrange{eq:eulerEquations}{eq:convectiveFluxes}, $\gls{x_vec}=(\x_1,\, \x_2)\Tr \in \mathbb{R}^2$ is the spatial coordinate vector, $\gls{t} \in \mathbb{R}^+$ is the time, $\gls{rho} \in \mathbb{R}^+$ is the fluid density, $\gls{m_vec}=(\gls{rho} \velocity_1,\, \gls{rho} \velocity_2)\Tr \in \mathbb{R}^2$ is the momentum vector, $\gls{u_vec}=(\velocity_1,\, \velocity_2)\Tr \in \mathbb{R}^2$ is the velocity vector, $\gls{rhoE} \in \mathbb{R}^+$ is the total energy, and $\gls{p} \in \mathbb{R}^+$ is the pressure. The total energy is the sum of the internal energy~$\gls{rhoe} \in \mathbb{R}^+$ and the kinetic energy so that
\begin{align}
	\underbrace{\gls{rhoE}}_\textrm{total energy} = \underbrace{\gls{rhoe}}_\textrm{internal energy} + \underbrace{\frac{1}{2} \gls{rho} \, \vec{\velocity} \cdot \vec{\velocity}}_\textrm{kinetic energy}\,.
\end{align}
The specific total energy is denoted by~$\gls{E} \in \mathbb{R}^+$, the specific internal energy by~$\gls{e} \in \mathbb{R}^+$, and the specific enthalpy by~$\gls{h} = \gls{e} + \gls{p}/\gls{rho}\,\in \mathbb{R}^+$. \emph{Specific} quantities are defined per unit mass. These expressions are usually used as synonyms in the literature.

Equation system~\labelcref{eq:eulerEquations} is not closed yet, since it misses an \gls{eos} for the pressure~$\gls{p} = \gls{p}(\gls{rho}, \gls{e})$, which we introduce in the next section.

 
\subsection{Shock Wave Reflections}	\label{sec:shock_wave_reflections}
The first discovery of the shock wave reflection phenomenon is attributed to Ernst Mach (\citeyear{mach1878}). He investigated two different configurations: a two-wave configuration, which is today known as a regular reflection and a three-wave configuration, which was later named after him as \emph{Mach reflection}. We refer to \textcite{ben-dor2007} for a detailed introduction into the topic. In general, shock wave reflections can be divided into \emph{regular} and \emph{irregular reflections}, as shown in \cref{fig:shock_reflections}.
\begin{figure}[tbp]
	\centering
	\def\svgwidth{0.45\textwidth}
	\subfloat[Regular reflection.\label{fig:shock_reflections_a}]{\input{../figures/regular_reflection.pdf_tex}}\hfil
	\def\svgwidth{0.45\textwidth}
	\subfloat[Irregular reflection (Mach reflection).\label{fig:shock_reflections_b}]{\input{../figures/irregular_reflection.pdf_tex}}
	\caption
	[General types of shock wave reflections.]
	{General types of shock wave reflections. (a) The incident shock wave hits the surface and creates a reflected shock. (b) The irregular reflection can be divided into four sub-types of which we show the standard solution of the three-shock theory~\parencite{vonneumann1961a}, also known as a \emph{Mach reflection}~\parencite[adapted from][Figures~1.1 and~1.2]{ben-dor2007}.}
	\label{fig:shock_reflections}
\end{figure}

\begin{figure}[tbp]
	\centering
	
	\subfloat[Entire domain, line-out along $\x_2=0.5$.]{
		\begin{tikzpicture}
			\begin{axis}[
				xlabel = $\x_1$,
				ylabel = Density~\gls{rho},
				xmajorgrids,
				ymajorgrids,
				xmin=0, xmax=1,
				ymin=0,
				xtick={0.0,0.2,...,1.0},
				ytick={0.0,0.2,...,1.0}
				]
				\addplot+[no marks] table {../data/shockTube_exact.txt};
				
%				\addplot+[no marks] table {../data/shockTube_p2_50x50cells_AB3.txt};
				\addplot+[no marks, red] table {../data/shockTube_p3_50x50cells_AB3.txt};
				\addplot+[no marks, blue] table {../data/shockTube_p4_50x50cells_AB3.txt};
				
				\addplot[only marks, every mark/.append style={solid, fill=gray}, mark=*, mark size = 1.2pt, mark repeat=50] table {../data/revision_sec6.1/ST_p2_xCells50_yCells50_s0=1.0E-03_dtFixed0_CFLFrac0.1_ALTS3_3_re1_subs0.txt};
				\addplot[only marks, every mark/.append style={solid, fill=red}, mark=square*, mark size = 1.2pt, mark repeat=50] table
				{../data/revision_sec6.1/ST_p3_xCells50_yCells50_s0=1.0E-03_dtFixed0_CFLFrac0.1_ALTS3_3_re1_subs0.txt};
				\addplot[only marks, every mark/.append style={solid, fill=blue}, mark=diamond*, mark size = 1.2pt, mark repeat=50] table {../data/revision_sec6.1/ST_p4_xCells50_yCells50_s0=1.0E-03_dtFixed0_CFLFrac0.1_ALTS3_3_re1_subs0.txt};
			\end{axis}
		\end{tikzpicture}
	}\hfil
	\subfloat[Zoom-in around the shock wave.]{
		\begin{tikzpicture}
			\begin{axis}[
				xlabel = $\x_1$,
				ylabel = Density~\gls{rho},
				xmajorgrids,
				ymajorgrids,
				xmin=0.9, xmax=1.0,
				ymin=0.1,
				xtick={0.9,0.925,0.95,0.975,1.0},
				ytick={0.1,0.15,0.2,0.25},
				x tick label style={
					/pgf/number format/.cd,
					precision=3,
					/tikz/.cd
				}
				]
				\addplot+[no marks] table {../data/shockTube_exact_zoom.txt};
				\addlegendentry{Exact};				
				
				\addplot+[no marks] table {../data/shockTube_p2_50x50cells_AB3.txt};
				\addlegendentry{$\gls{poly_degree}=2$};
				\addplot+[no marks, red] table {../data/shockTube_p3_50x50cells_AB3.txt};
				\addlegendentry{$\gls{poly_degree}=3$};				
				\addplot+[no marks, blue] table {../data/shockTube_p4_50x50cells_AB3.txt};
				\addlegendentry{$\gls{poly_degree}=4$};								
				
				\addplot[only marks, every mark/.append style={solid, fill=gray}, mark=*, mark size = 1.2pt, mark repeat=5] table {../data/shockTube_p2_50x50cells_ALTS3_3_re1_subs0.txt};
				\addlegendentry{ALTS~($\gls{poly_degree} = 2$)}
				\addplot[only marks, every mark/.append style={solid, fill=red}, mark=square*, mark size = 1.2pt, mark repeat=5] table
				{../data/revision_sec6.1/ST_p3_xCells50_yCells50_s0=1.0E-03_dtFixed0_CFLFrac0.1_ALTS3_3_re1_subs0.txt};
				\addlegendentry{ALTS~($\gls{poly_degree} = 3$)}		
				\addplot[only marks, every mark/.append style={solid, fill=blue}, mark=diamond*, mark size = 1.2pt, mark repeat=5] table {../data/revision_sec6.1/ST_p4_xCells50_yCells50_s0=1.0E-03_dtFixed0_CFLFrac0.1_ALTS3_3_re1_subs0.txt};
				\addlegendentry{ALTS~($\gls{poly_degree} = 4$)}
			\end{axis}
		\end{tikzpicture}
	}	
	\caption
	[Sod shock tube problem. $\gls{poly_degree}$-refinement study using the two-step shock-capturing strategy.]
	{Sod shock tube problem. $\gls{poly_degree}$-refinement study using the two-step shock-capturing strategy. We show the solution for a global Adams-Bashforth scheme of order~$\gls{order_AB}=3$ at~$\gls{t_end} = 0.25$. Additionally, we present comparison results for the \acrfull{alts} scheme of the same temporal order~\parencite[adapted from][Figure~5]{geisenhofer2019}.}
	%	, an initial number of clusters of $\gls{init_num_clusters} = 3$, and a reclustering interval of~$\gls{reclust_int}=1$
	\label{fig:shocktube_pstudy}
\end{figure}